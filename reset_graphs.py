import sys
try:
    import requests
    import os
except ImportError:
    sys.exit('One of the modules could not be imported. Make sure you already installed the dependencies using "pip3 install -r requirements.txt". If not, please install them before running the RT-Processing.')
    

if not os.path.exists('.api_config'):
    sys.exit('Please perform the api configuration using the "unix_configure.sh" or the "win_configure.cmd" before running the RT-Processing.')


api_config = open('.api_config', 'r').readlines()
api_url = api_config[0].rstrip() + "/reset"
api_key = api_config[1].rstrip()

request_object = {
    "key": api_key
}

if requests.post(api_url, json=request_object).status_code == 200:
    print("Successfully updated website.")
else:
    print("Update failed, please retry. (possible reason: outdated api url/key)")
