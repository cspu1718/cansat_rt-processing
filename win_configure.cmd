@echo off
set /p apiUrl="Enter the API URL: "
set /p apiPwd="Enter the API password: "
(
	echo %apiUrl%
	echo %apiPwd%
) > .api_config
set apiUrl=""
set apiPwd=""
@echo on
