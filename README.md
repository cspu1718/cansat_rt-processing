# Torus RT-Processing
Here, you will find the usage instructions for the Torus RT-Processing.

## Prerequisites
### Python 3
#### On Windows
Python can be installed on Windows using the Microsoft Store. Simply search "Python" on the Microsoft Store and if multiple versions are available, select the latest one (and make sure the publisher of the package you install is "Python Software Foundation").
#### On Mac
Python can be installed on Mac using Homebrew using the following command: `brew install python` (if Homebrew is not installed yet, make sure it is before running the command)
#### On Linux
Python should already be installed on Linux, but if it isn't, you can do so using your package manager. For APT: `sudo apt install python3`
### Driver for the PH2303HX TTL-USB converter
The installation instructions can be found in section 5 of the "RF-TranscieverDesign.docx" document, available in the following directory on sharepoint: "CanSat0000_CSPU/3200"

## Setup
The setup only needs to be performed once when installing and should preferably be run again each time there is an update to the software.

1. Clone the repository (using Sourcetree, for example).
2. Open the terminal/command prompt and navigate to the folder where the repository is cloned.
3. Type `pip3 install -r requirements.txt` and hit enter.

## API configuration
Before running the RT-Processing, you need to configure the api url and password. Here is how to do it.

_Note: you should repeat the configuration process every time the api url and/or password changes._

1. Open the terminal/command prompt and navigate to the folder where the repository is cloned.
2. If you're on Unix (Mac or Linux), type `source unix_configure.sh` and hit enter. If you're on Windows, type `win_configure.cmd` and hit enter.
3. Follow the instructions given by the program to configure the api url and password.

## Running the RT-Processing
Make sure you have already performed the setup on your machine (as outlined in the previous section) before proceeding to this section.

1. Open the terminal/command prompt and navigate to the folder where the repository is cloned.
2. Type `python3 torus_rt-processing.py` and hit enter.

## Platforms
Windows | Mac | Linux
--- | --- | ---
Compatible and tested. | Compatible but to be tested - you never know with Macs... ;) | The software is compatible but the PH2303HX TTL-USB driver is unavailable.

## For advanced users
Advanced users might want to consider using a virtual environment for managing dependencies. If you are using a virtual environment, store it in the `.venv` folder at the root of the repository, as that location is included in .gitignore and won't be tracked by your version control system.
