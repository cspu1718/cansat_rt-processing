import sys
import time
try:
    import requests
    import serial
    import serial.tools.list_ports
    import copy
    from multiprocessing.dummy import Pool
    import os
except ImportError:
    sys.exit('One of the modules could not be imported. Make sure you already installed the dependencies using "pip3 install -r requirements.txt". If not, please install them before running the RT-Processing.')
    

if not os.path.exists('.api_config'):
    sys.exit('Please perform the api configuration using the "unix_configure.sh" or the "win_configure.cmd" before running the RT-Processing.')


api_config = open('.api_config', 'r').readlines()
api_url = api_config[0].rstrip() + "/addData"
api_key = api_config[1].rstrip()

print("Testing connection to server.")
if requests.post(api_config[0].rstrip()+"/modifyConfig", json={"key":api_key,"data":{"param": "testparam","value": "test"}}).status_code == 200:
    print("Connection to website successsful.")
else:
    sys.exit('Connection to website unsuccessful (possible reason: outdated api url/key). Please check the credentials and rerun the program.')

keep_record_every_x_seconds = 0.7 # do not keep every record, only keep one in X
records_per_request = 5 # how many records to include in a single request
use_thermistor_3 = True

template_request_object = {
    "key": api_key,
    "data": []
}

curr_request_object = copy.deepcopy(template_request_object)
last_record_ts = 0

requests_pool = Pool()

ser = serial.Serial()
ser.baudrate = 115200
serial_setup_ok = False

while not serial_setup_ok:
    try:
        ports = [port for port in serial.tools.list_ports.comports() if port[2] != 'n/a']
        if not ports:
            print("Currently no COM ports available. Make sure the RT-Processing is connected and press enter to scan the ports again.")
            input()
            continue

        print("Here is a list of available COM ports:")
        for port in ports:
            print("  -", port)
        ser.port = input("Please choose a port: ")
        ser.open()
        serial_setup_ok = True
    except serial.SerialException as e:
       print("Couldn't connect to serial.", e)

print("Torus RT Processing started. CTRL+C to interrupt.")

try:
    while True:
        try:
            line = ser.readline().decode().strip()
        except UnicodeDecodeError as e:
            print("Error: couldn't decode some data arriving on the serial port. Skipping line.")
            print(e)
            continue

        if time.time() - last_record_ts < keep_record_every_x_seconds: continue

        last_record_ts = time.time()
        
        record = line.split(',')
        
        try:
            record_ts = int(record[0])
            record_alt = float(record[7])-float(record[8])
            record_vel = float(record[9])
            record_temp = float(record[10+2*use_thermistor_3])
            record_len = float(record[12+use_thermistor_3])
        except:
            print("Invalid record. Ignoring.")
            print("Record received: ", record)
            continue

        curr_request_object["data"].append({
            "ts": record_ts,
            "alt": record_alt,
            "vel": record_vel,
            "temp": record_temp,
            "len": record_len
        })

        if len(curr_request_object["data"]) >= records_per_request:
            requests_pool.apply_async(requests.post, (api_url,), {'json': curr_request_object})
            curr_request_object = copy.deepcopy(template_request_object)
            
            print("Sent a request to the server.")
            if ser.in_waiting > 100:
                print(f"Warning: processor overloaded ({ser.in_waiting} bytes waiting on serial)")
        
except KeyboardInterrupt:
    ser.close()
    print("Exiting.")
    
